import React from "react";
import "./App.css";
import Todo from "./components/Todo.jsx";

function App() {
  return (
    <div className="App">
      <h1>Hello Todo List</h1>
      <Todo />
    </div>
  );
}

export default App;
