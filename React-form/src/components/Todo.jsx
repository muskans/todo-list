import React, { Component } from "react";

class Todolist extends Component {
  constructor(prop) {
    super(prop);
    this.state = { item: "", todoList: [] };
    //this.handleChange = this.handleChange.bind(this);
    //this.addData = this.addData.bind(this);
  }

  handleChange = e => {
    var inputValue = e.target.value;
    this.setState({
      item: inputValue
    });
  };
  addData = e => {
    e.preventDefault();
    var inputValue = this.state.item;
    var inputtext2 = this.state.todoList;
    inputtext2.push(inputValue);
    this.setState({
      todoList: inputtext2,
      item: ""
    });
  };

  delete = e => {
    var id = e.target.id;
    var inputtext2 = this.state.todoList;
    inputtext2.splice(id, 1);
    this.setState({
      todoList: inputtext2
    });
  };

  render() {
    var newList = this.state.todoList.map((e, i) => (
      <li key={i} style={{ color: "blue", fontSize: "20px" }}>
        {e}

        <span onClick={this.delete} id={i}>
          <button style={{ marginLeft: "20px", color: "red" }}>Remove</button>
        </span>
        <br></br>
        <br></br>
      </li>
    ));
    return (
      <form>
        <input
          type="text"
          value={this.state.item}
          onChange={this.handleChange}
        />
        <button onClick={this.addData}>Add</button>
        <ul>{newList}</ul>
      </form>
    );
  }
}

export default Todolist;
